# FAQ Bonobros

## C'est Narkuss qui code ? De qui est constituée l'équipe ?

Non, il s'en entouré de devs et de graphistes, lui invente le jeu.
Il y a actuellement deux developpeurs et deux graphistes, dont certains on pu passer à plein temps sur Bonobros.

## Le jeux est-il payant ?

Non, il est free to play.

## Je veux jouer la béta, mais il me dit que le jeu n'est pas disponible !

Il faut cliquer sur "demander l'accès" un peu plus bas dans la page du jeu, il est donné automatiquement.

## Comment allez-vous gagner de l'argent ?

Le même modèle que LoL : il y aura des skins payant, et la possibilité de débloquer plus vite des personnages en les achetant.

## Le jeu est-il / sera-t-il disponible sur Mac / Linux / Android / IPhone / Swtich / Psx ?

On l'espère, mais le temp et le cout de dévelopement est important. Dans un premier l'équipe se concentre sur la version Windows.

## Il y aura un algorithme pour le matchmaking ?

L'algorithme cherche seulement à mettre en relation les joueurs avec le niveau le plus proche. Passé un certain temps d'attente, l'algorithme augmente l'écart de niveau possible.

## Le jeu est développé avec quel moteur ?

Unreal Engine 5